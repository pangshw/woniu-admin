# woniu-admin

#### 介绍
spring boot、mybatis-plus

#### 软件架构
软件架构说明

#### 0.1.4版本
1. 接口参数校验
2. 统一异常拦截

#### 0.1.3版本
1. 统一返回值

#### 0.1.2版本
1. restful与rpc

#### 0.1.1版本
1. 支持mybatis-plus
2. /GeneratorMySQL/GeneratorMySQL生成entity、mapper、service代码

#### 0.1版本
1. 支持swagger 3

```
http://localhost:8080/doc.html
http://localhost:8080/swagger-ui/index.html
```
#### swagger3
| swagger2 |	OpenAPI 3 | 注解位置                         |
| --- | -------- |------------------------------|
| @Api | @Tag(name = “接口类描述”) | Controller 类上                |
| @ApiOperation | @Operation(summary =“接口方法描述”) | Controller 方法上               |
| @ApiImplicitParams | @Parameters | Controller 方法上               |
| @ApiImplicitParam | @Parameter(description=“参数描述”) | Controller 方法上 @Parameters 里 |
| @ApiParam | @Parameter(description=“参数描述”) | Controller 方法的参数上            |
| @ApiIgnore | @Parameter(hidden = true) 或 @Operation(hidden = true) 或 @Hidden | -                            |
| @ApiModel | @Schema | DTO类上                |
| @ApiModelProperty | @Schema | DTO属性上                |

#### git常用命令
```
git tag -a v0.1.4 -m "接口参数校验、统一异常拦截"
git push origin tag v0.1.4
git push origin --tags

git tag -a v0.1.3 -m "统一返回值"
git push origin tag v0.1.3
git push origin --tags

git tag -a v0.1.2 -m "restful与rpc"
git push origin tag v0.1.2
git push origin --tags

git tag -a v0.1.1 -m "支持mybatis-plus，生成entity、mapper、service代码"
git push origin tag v0.1.1
git push origin --tags

git tag -a v0.1 -m "初始化工程，spring boot 3 + swagger 3"
git push origin tag v0.1
git push origin --tags
```