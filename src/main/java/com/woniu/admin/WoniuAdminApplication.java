package com.woniu.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.woniu")
public class WoniuAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(WoniuAdminApplication.class, args);
    }

}
