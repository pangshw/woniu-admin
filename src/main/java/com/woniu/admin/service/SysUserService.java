package com.woniu.admin.service;

import com.woniu.admin.entity.SysUserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 蜗牛数字化
 * @since 2023-06-18
 */
public interface SysUserService extends IService<SysUserEntity> {

}
