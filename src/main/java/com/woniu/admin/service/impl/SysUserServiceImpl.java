package com.woniu.admin.service.impl;

import com.woniu.admin.entity.SysUserEntity;
import com.woniu.admin.mapper.SysUserMapper;
import com.woniu.admin.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 蜗牛数字化
 * @since 2023-06-18
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {

}
