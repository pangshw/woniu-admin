package com.woniu.admin.mapper;

import com.woniu.admin.entity.SysUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 蜗牛数字化
 * @since 2023-06-18
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

}
